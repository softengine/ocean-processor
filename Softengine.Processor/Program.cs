﻿using Softengine.Processor.Core.Logging;
using Softengine.Processor.Core.Queue;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Softengine.Processor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            Logger.Current.LogInformation("-------------------------------------------------------------");
            Logger.Current.LogInformation("Softengine Processor - Started");

            List<Core.Queue.Message> workload = null;
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                string type = GetArgument(args, 1);
                string action = GetArgument(args, 2);
                string docEntry = GetArgument(args, 3);
                workload = new Manager().Workload(type, action, docEntry);
            }
            else
            {
                workload = new Manager().Workload();
            }

            try { foreach(var message in workload) message.Process(); } catch (Exception ex) { Logger.Current.Log(ex);}

            Logger.Current.LogInformation("Softengine Processor - Completed");
        }

        private static string GetArgument(string[] args, int index)
        {
            try
            {
                return args[index];
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
