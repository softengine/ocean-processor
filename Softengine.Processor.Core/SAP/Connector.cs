﻿using Softengine.Processor.Core.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Softengine.Processor.Core.SAP
{
    public static class Connector
    {

        private static Configuration.ProcessorConfiguration _configuration;
        private static SAPbobsCOM.Company _company;
        public static Dictionary<string, Assembly> AssemblyCache;

        public static Configuration.ProcessorConfiguration Settings
        {
            get
            {
                if (_configuration == null)
                {
                    if (AssemblyCache == null) AssemblyCache = new Dictionary<string, Assembly>();
                    bool needsSaving = false;
                    var path = Assembly.GetEntryAssembly().Location.Replace(Assembly.GetEntryAssembly().ManifestModule.Name, "");
                    path = Path.Combine(path, "ControllerConfiguration.xml");

                    var serializer = new XmlSerializer(typeof(Configuration.ProcessorConfiguration));
                    using (var fileStream = new FileStream(path, FileMode.Open))
                    {
                        _configuration = (Configuration.ProcessorConfiguration)serializer.Deserialize(fileStream);
                        if (!_configuration.SAP.Password.IsEcrypted())
                        {
                            _configuration.SAP.Password = _configuration.SAP.Password.Encrypt();

                            // Save New Configuration
                            needsSaving = true;
                        }
                    }

                    if (needsSaving)
                    {
                        var xmlDocument = new XmlDocument();
                        serializer = new XmlSerializer(_configuration.GetType());
                        var ns = new XmlSerializerNamespaces();
                        ns.Add("", "");
                        var sw = new StringWriter();
                        var xmlWriter = XmlWriter.Create(sw, new XmlWriterSettings() { OmitXmlDeclaration = true });

                        serializer.Serialize(xmlWriter, _configuration, ns);
                        string xml = sw.ToString();
                        File.WriteAllText(path, xml.PrintXML());
                    }
                    _configuration.SAP.Password = _configuration.SAP.Password.Decrypt();
                }

                return _configuration;
            }
        }

        public static SAPbobsCOM.Company Company()
        {
            try
            {
                _company = new SAPbobsCOM.Company
                {
                    CompanyDB = Settings.SAP.CompanyDB,
                    Server = Settings.SAP.Server,
                    UserName = Settings.SAP.UserName,
                    Password = Settings.SAP.Password
                };

                switch (Settings.SAP.DBServerType)
                {
                    case "dst_MSSQL2008": _company.DbServerType = (SAPbobsCOM.BoDataServerTypes)6;   break;
                    case "dst_MSSQL2012": _company.DbServerType = (SAPbobsCOM.BoDataServerTypes)7;   break;
                    case "dst_MSSQL2014": _company.DbServerType = (SAPbobsCOM.BoDataServerTypes)8;   break;
                    case "dst_HANADB":    _company.DbServerType = (SAPbobsCOM.BoDataServerTypes)9;      break;
                    case "dst_MSSQL2016": _company.DbServerType = (SAPbobsCOM.BoDataServerTypes)10;   break;
                    case "dst_MSSQL2017": _company.DbServerType = (SAPbobsCOM.BoDataServerTypes)11;   break;
                    default: throw new Exception("Unsupported version of SAP.");
                }

                if (_company.Connect() != 0) throw new Exceptions.SAPException(_company); else return _company;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
