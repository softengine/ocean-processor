﻿using System.Diagnostics;

namespace Softengine.Processor.Core.Queue
{
    public class Message
    {
        private readonly string _successQuery;
        private readonly string _failureQuery;
        public readonly SAPbobsCOM.Company Company;

        public Message(string successQuery, string failureQuery, SAPbobsCOM.Company company)
        {
            _successQuery = successQuery;
            _failureQuery = failureQuery;
            Company = company;
        }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Action { get; set; }
        public string Key { get; set; }

        public void Success()
        {
            var rsCloseQueue = Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rsCloseQueue.DoQuery(string.Format(_successQuery, Id));

            Logger.LogInformation($"Message {Id} processed successfully");
        }

        public void Failed(int errorCode, string errorMessage)
        {
            var rsCloseQueue = Company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rsCloseQueue.DoQuery(string.Format(_failureQuery, Id, errorCode, errorMessage.Replace("'", "''")));

            Logger.LogError($"Message {Id}, failed with error: {errorMessage}");
        }

        public Logging.ILogger Logger
        {
            get { return Logging.Logger.Current; }
        }
        public void Process()
        {
            var controller = SAP.Connector.Settings.Controller(Type, Action);
            if (controller == null)
            {
                Logger.LogWarning($"Message {Id} of type {Type} and action {Action}, is not configured with a controller. Message {Id} will be ignored.");
                return;
            }
            var method = controller.CurrentMethod();
            if (method == null)
            {
                Logger.LogWarning($"Message {Id} of type {Type} and action {Action}, is configured with a method {controller.Method}, the method is not found. Message {Id} will be ignored.");
                return;
            }

            Logger.LogDebug($"Processing Message id {Id}, type {Type},  action {Action} and key {Key}");

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            method.Invoke(null, new object[] { this });
            stopWatch.Stop();
            var ts = stopWatch.Elapsed;
            var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Logger.LogDebug($"Executed method {controller.ClassName}.{controller.Method} in {elapsedTime}");
        }
    }
}
