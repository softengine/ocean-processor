﻿using Softengine.Processor.Core.SAP;
using System.Collections.Generic;

namespace Softengine.Processor.Core.Queue
{
    public class Manager
    {
        private readonly string Query;
        private readonly string SuccessQuery;
        private readonly string FailureQuery;
        private readonly string FieldId;
        private readonly string FieldKey;
        private readonly string FieldType;
        private readonly string FieldAction;
        public Manager()
        {
            SuccessQuery = Connector.Settings.DatabaseProcedures.SuccessQuery;
            FailureQuery = Connector.Settings.DatabaseProcedures.FailureQuery;
            Query = Connector.Settings.DatabaseProcedures.MainQuery;
            FieldId = Connector.Settings.DatabaseProcedures.Id;
            FieldKey = Connector.Settings.DatabaseProcedures.Key;
            FieldType = Connector.Settings.DatabaseProcedures.Type;
            FieldAction = Connector.Settings.DatabaseProcedures.Action;
        }

        public List<Message> Workload(string type, string action, string key = "")
        {
            var list = new List<Message>();

            var company = Connector.Company();
            list.Add(
                   new Message(SuccessQuery, FailureQuery, company)
                   {
                       Id = -1,
                       Key = (string.IsNullOrEmpty(key)) ? "-1": key,
                       Action = action,
                       Type = type
                   });

            return list;
        }

        public List<Message> Workload()
        {
            var list = new List<Message>();

            var company = Connector.Company();

            var rs = company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            rs.DoQuery(Query);
            while (!rs.Eof)
            {
                list.Add(
                    new Message(SuccessQuery, FailureQuery, company)
                    {
                        Id = int.Parse(rs.Fields.Item(FieldId).Value.ToString()),
                        Key = rs.Fields.Item(FieldKey).Value.ToString(),
                        Action = rs.Fields.Item(FieldAction).Value.ToString(),
                        Type = rs.Fields.Item(FieldType).Value.ToString()
                    });
                rs.MoveNext();
            }

            return list;
        }
    }
}
