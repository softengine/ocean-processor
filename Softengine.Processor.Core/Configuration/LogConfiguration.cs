﻿using System.Xml.Serialization;

namespace Softengine.Processor.Core.Configuration
{
	[XmlRoot(ElementName = "Log")]
	public class LogConfiguration
	{
		[XmlAttribute(AttributeName = "type")]
		public string Type { get; set; }
		[XmlAttribute(AttributeName = "path")]
		public string Path { get; set; }

		[XmlAttribute(AttributeName = "fileName")]
		public string FileName { get; set; }
		
		[XmlAttribute(AttributeName = "cleanUpPattern")]
		public string CleanUpPattern { get; set; }

		[XmlAttribute(AttributeName = "maxDays")]
		public string MaxDays { get; set; }

		[XmlAttribute(AttributeName = "level")]
		public string Level { get; set; }
	}
}
