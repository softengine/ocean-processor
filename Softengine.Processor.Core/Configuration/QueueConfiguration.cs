﻿using System.Xml.Serialization;

namespace Softengine.Processor.Core.Configuration
{
	[XmlRoot(ElementName = "Queue")]
	public class QueueConfiguration
	{
		[XmlAttribute(AttributeName = "type")]
		public string Type { get; set; }

		[XmlAttribute(AttributeName = "action")]
		public string Action { get; set; }

		[XmlAttribute(AttributeName = "method")]
		public string Method { get; set; }

		[XmlAttribute(AttributeName = "class")]
		public string ClassName { get; set; }
	}
}
