﻿using System.Xml.Serialization;

namespace Softengine.Processor.Core.Configuration
{
	[XmlRoot(ElementName = "SAP")]
	public class SAPConfiguration
	{
		[XmlAttribute(AttributeName = "server")]
		public string Server { get; set; }
		[XmlAttribute(AttributeName = "companyDB")]
		public string CompanyDB { get; set; }
		[XmlAttribute(AttributeName = "dBServerType")]
		public string DBServerType { get; set; }
		[XmlAttribute(AttributeName = "userName")]
		public string UserName { get; set; }
		[XmlAttribute(AttributeName = "password")]
		public string Password { get; set; }
	}
}
