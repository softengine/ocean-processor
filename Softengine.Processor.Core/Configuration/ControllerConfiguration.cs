﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Softengine.Processor.Core.Configuration
{
	[XmlRoot(ElementName = "Controller")]
	public class ControllerConfiguration
	{
		[XmlElement(ElementName = "Queue")]
		public List<QueueConfiguration> Queues { get; set; }
		[XmlAttribute(AttributeName = "path")]
		public string Path { get; set; }


	}
}
