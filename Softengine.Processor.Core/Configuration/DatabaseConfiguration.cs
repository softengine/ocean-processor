﻿using System.Xml.Serialization;

namespace Softengine.Processor.Core.Configuration
{
	[XmlRoot(ElementName = "DatabaseProcedures")]
	public class DatabaseConfiguration
	{
		[XmlAttribute(AttributeName = "mainQuery")]
		public string MainQuery { get; set; }

		[XmlAttribute(AttributeName = "successQuery")]
		public string SuccessQuery { get; set; }

		[XmlAttribute(AttributeName = "failureQuery")]
		public string FailureQuery { get; set; }

		[XmlAttribute(AttributeName = "idFieldName")]
		public string Id { get; set; }

		[XmlAttribute(AttributeName = "keyFieldName")]
		public string Key { get; set; }

		[XmlAttribute(AttributeName = "actionFieldName")]
		public string Action { get; set; }

		[XmlAttribute(AttributeName = "typeFieldName")]
		public string Type { get; set; }
	}
}
