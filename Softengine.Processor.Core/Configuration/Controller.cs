﻿using System;
using System.Reflection;

namespace Softengine.Processor.Core.Configuration
{
    public class Controller
    {
        public Controller(string path, string className, string method)
        {
            Path = path;
            Method = method;
            ClassName = className;
        }
        public string Path { get; set; }
        public string Method { get; set; }
        public string ClassName { get; set; }

        private Assembly CurrentAssembly()
        {
            if (!SAP.Connector.AssemblyCache.ContainsKey(Path))
            {
                // Load Assembly and added to the cache
                var assembly = Assembly.LoadFile(Path);
                SAP.Connector.AssemblyCache.Add(Path, assembly);
            }

            return SAP.Connector.AssemblyCache[Path];
        }

        internal MethodInfo CurrentMethod()
        {
            var controller = CurrentAssembly();
            if (controller == null) return null;

            foreach (Type type in controller.GetTypes())
            {
                if (type.FullName != ClassName) continue;
                try { return type.GetMethod(Method); } catch { return null; }
            }
            return null;

        }
    }
}
