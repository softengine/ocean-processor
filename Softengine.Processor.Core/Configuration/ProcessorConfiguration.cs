﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Softengine.Processor.Core.Configuration
{
	[XmlRoot(ElementName = "Configuration")]
	public class ProcessorConfiguration
	{
		[XmlElement(ElementName = "SAP")]
		public SAPConfiguration SAP { get; set; }

		[XmlElement(ElementName = "DatabaseProcedures")]
		public DatabaseConfiguration DatabaseProcedures { get; set; }

		[XmlElement(ElementName = "Log")]
		public LogConfiguration Log { get; set; }

		[XmlElement(ElementName = "Controller")]
		public List<ControllerConfiguration> Controllers { get; set; }

		public Controller Controller(string type, string action)
        {
			foreach(var controller in Controllers)
            {
				foreach (var queue in controller.Queues)
				{
					if ((queue.Type == type) && (queue.Action == action))
						return new Controller(controller.Path,  queue.ClassName, queue.Method);
				}
			}
			return null;
        }

	}
}
