﻿using System;
using System.IO;

namespace Softengine.Processor.Core.Logging
{
    public static class Logger
    {
        private static BaseLogger _currentLogger;
        private static string _loggerType;

        private static void CleanUpFolder()
        {
            var folder = new DirectoryInfo(_currentLogger.FolderPath);
            var files = folder.GetFiles(_currentLogger.CleanUpPattern);

            foreach (FileInfo file in files) if (file.CreationTime < DateTime.Now.AddDays(-_currentLogger.MaxDays)) file.Delete();
        }

        public static ILogger Current
        {
            get
            {
                if (string.IsNullOrEmpty(_loggerType)) _loggerType = SAP.Connector.Settings.Log.Type;



                if (_currentLogger == null)
                {
                    switch (_loggerType.ToUpper())
                    {
                        case "XML": _currentLogger = new XMLLogger();  break;
                        case "TXT": _currentLogger = new TextLogger(); break;
                        case "CSV": _currentLogger = new CSVLogger();  break;
                        default: throw new Exception("Invalid logger type, compatible types (XML, CSV, TEXT)");
                    }

                    switch (SAP.Connector.Settings.Log.Level)
                    {
                        case "Info":
                            _currentLogger.LogLevel = LogLevel.Info; break;
                        case "Warn":
                            _currentLogger.LogLevel = LogLevel.Warn; break;
                        case "Error":
                            _currentLogger.LogLevel = LogLevel.Error; break;
                        case "Exception":
                            _currentLogger.LogLevel = LogLevel.Exception; break;
                        default:
                            _currentLogger.LogLevel = LogLevel.Debug; break;
                    }

                    CleanUpFolder();
                }

                return _currentLogger;
            }
        }
    }
}
