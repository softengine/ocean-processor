﻿using System;
using System.Collections.Generic;

namespace Softengine.Processor.Core.Logging
{
    public interface ILogger
    {
        //void Log(string logtype, IEnumerable<string> messages);
        //void Log(string logtype, string message);
        void Log(Exception ex);
        void LogDebug(string message);
        void LogDebug(IEnumerable<string> messages);
        void LogInformation(string message);
        void LogInformation(IEnumerable<string> messages);
        void LogWarning(string message);
        void LogWarning(IEnumerable<string> messages);
        void LogError(string message);
        void LogError(IEnumerable<string> messages);
    }
}
