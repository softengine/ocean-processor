﻿using System;


namespace Softengine.Processor.Core.Logging
{
    public class TextLogger : StreamLogger, ILogger
    {
        public override string LogLine(string logtype, string message)
        {
            return (string.IsNullOrEmpty(logtype)) ?
                $"{DateTime.Now:hh:mm:ss} ==>       {message}": 
                $"{DateTime.Now:hh:mm:ss} ==> {logtype}: {message}";
        }

        public override string FileExtension()
        {
            return "TXT";
        }

    }
}
