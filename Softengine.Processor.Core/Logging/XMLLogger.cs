﻿using System;
using System.Collections.Generic;
using System.IO;

using System.Xml;

namespace Softengine.Processor.Core.Logging
{
    public class XMLLogger : BaseLogger, ILogger
    {
        private static XmlDocument _doc = new XmlDocument();

        private void LoadDocument()
        {
            if (File.Exists(FileName))
                _doc.Load(FileName);
            else
            {
                var root = _doc.CreateElement("logs");
                _doc.AppendChild(root);
            }
        }

        private XmlElement InitializeElement(string logtype)
        {
            var el = (XmlElement)_doc.DocumentElement.AppendChild(_doc.CreateElement("log"));
            el.SetAttribute("type", logtype);
            el.SetAttribute("timestamp", DateTime.Now.ToLongTimeString());
            return el;
        }

        private void Element(string logtype, string message )
        {
            var el = InitializeElement(logtype);
            el.SetAttribute("message", message);
        }
        private void Element(string logtype, IEnumerable<string> messages)
        {
            var el = InitializeElement(logtype);
            foreach (var message in messages)
                el.AppendChild(_doc.CreateElement("Message")).InnerText = message;
        }
        private void Element(Exception ex)
        {
            var el = InitializeElement("Exception");
            el.AppendChild(_doc.CreateElement("Message")).InnerText = ex.Message;
            el.AppendChild(_doc.CreateElement("Source")).InnerText = ex.Source;
            el.AppendChild(_doc.CreateElement("StackTrace")).InnerText = ex.StackTrace;
        }

        protected override void Log(string logtype, IEnumerable<string> messages)
        {
            LoadDocument(); lock (Locker) { Element(logtype, messages); _doc.Save(FileName);}
        }
        protected override void Log(string logtype, string message)
        {
            LoadDocument(); lock (Locker) { Element(logtype, message); _doc.Save(FileName); }
        }

        internal override void Log(string message)
        {
            LoadDocument(); lock (Locker) { Element(string.Empty, message); _doc.Save(FileName); }
        }
        public override void Log(Exception ex)
        {
            LoadDocument(); lock (Locker) { Element(ex); _doc.Save(FileName); }


            if (ex.InnerException != null)
            {
                Log($"    INNER EXCEPTION");
                Log(ex.InnerException);
            }

        }
        public override string FileExtension()
        {
            return "XML";
        }
    }
}
