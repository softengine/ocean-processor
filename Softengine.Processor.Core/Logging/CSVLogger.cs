﻿using System;


namespace Softengine.Processor.Core.Logging
{
    public class CSVLogger : StreamLogger, ILogger
    {
        public override string LogLine(string logtype, string message)
        {
            return  (string.IsNullOrEmpty(logtype)) ? 
                $"{DateTime.Now:hh:mm:ss},,{message.Replace(",", "").Trim()}": 
                $"{DateTime.Now:hh:mm:ss},{logtype.Replace(",", "").Trim()},{message.Replace(",", "").Trim()}";
        }

        public override string FileExtension()
        {
            return "CSV";
        }

    }
}
