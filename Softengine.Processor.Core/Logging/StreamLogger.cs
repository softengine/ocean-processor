﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Softengine.Processor.Core.Logging
{
    public abstract class StreamLogger : BaseLogger
    {
        public override string FileExtension()
        {
            throw new NotImplementedException();
        }

        protected override void Log(string logtype, IEnumerable<string> messages)
        {
            FileStream fs = null;
            StreamWriter sr = null;

            lock (Locker)
            {
                try
                {
                    using (fs = GetFileStream())
                    {
                        using (sr = new StreamWriter(fs, Encoding.Default))
                        {
                            foreach (var message in messages)
                                sr.WriteLine(LogLine(logtype, message));
                            sr.Close();
                        }

                        fs.Close();
                    }
                }
                catch
                {
                }
                finally
                {
                    if (sr != null) sr.Close();
                    if (fs != null) fs.Close();
                }
            }
        }

        protected override void Log(string logtype, string message)
        {
            FileStream fs = null;
            StreamWriter sr = null;

            lock (Locker)
            {
                try
                {
                    using (fs = GetFileStream())
                    {
                        using (sr = new StreamWriter(fs, Encoding.Default))
                        {
                            sr.WriteLine(LogLine(logtype, message));
                            sr.Close();
                        }

                        fs.Close();
                    }
                }
                catch
                {
                }
                finally
                {
                    if (sr != null) sr.Close();
                    if (fs != null) fs.Close();
                }
            }
        }

        internal override void Log(string message)
        {
            Log("         ", message);
        }

        public override void Log(Exception ex)
        {
            Log("Exception", $"    Message: {ex.Message}");
            Log("         ", $"    Source : {ex.Message}");
            var lines = ex.StackTrace.Split(Environment.NewLine.ToCharArray());
            foreach (var line in lines)
            {
                if (line.Trim().Length > 0)
                    Log(string.Empty, $"                    {line}");
            }

            if (ex.InnerException != null)
            {
                Log(string.Empty, $"    INNER EXCEPTION");
                Log(ex.InnerException);
            }
        }

        public abstract string LogLine(string logtype, string message);

        private FileStream GetFileStream()
        {
            if (File.Exists(FileName))
                return File.Open(FileName, FileMode.Append);
            else
                return new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
        }
    }
}
