﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Softengine.Processor.Core.Logging
{
    public abstract class BaseLogger: ILogger
    {
        internal LogLevel LogLevel { get; set; }

        public static readonly object Locker = new object();
        private string _fileName = string.Empty;
        private string _cleanUpPattern = string.Empty;
        private string _folderPath = string.Empty;
        private string _companyName = string.Empty;
        private int _maxDays = -1;

        private string CompanyName
        {
            get
            {
                if (string.IsNullOrEmpty(_companyName)) _companyName = SAP.Connector.Settings.SAP.CompanyDB;
                return _companyName;
            }
        }

        public int MaxDays
        {
            get
            {
                if (_maxDays == -1) _maxDays = int.Parse(SAP.Connector.Settings.Log.MaxDays);
                return _maxDays;
            }
        }
        public string CleanUpPattern
        {
            get
            {
                if (string.IsNullOrEmpty(_cleanUpPattern)) _cleanUpPattern = SAP.Connector.Settings.Log.CleanUpPattern.Replace("{CompanyDb}", CompanyName).Replace("{LogType}", FileExtension());
                return _cleanUpPattern;
            }
        }
        protected string FileName
        {
            get
            {
                if (string.IsNullOrEmpty(_fileName))
                {
                    _fileName = string.Format(SAP.Connector.Settings.Log.FileName.Replace("{CompanyDb}", CompanyName).Replace("{LogType}", FileExtension()), DateTime.Now);
                    _fileName = Path.Combine(FolderPath, _fileName);
                }
                return _fileName;
            }
        }
        public string FolderPath
        {
            get
            {
                if (string.IsNullOrEmpty(_folderPath))
                {
                    _folderPath = string.Format(SAP.Connector.Settings.Log.Path.Replace("{CompanyDb}", CompanyName), DateTime.Now);
                    if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);
                }
                return _folderPath;
            }
        }

        public void Log(LogLevel level, string message) 
        { 
            switch (level)
            {
                case LogLevel.Debug:
                    LogDebug(message);
                    break;
                case LogLevel.Info:
                    LogInformation(message);
                    break;
                case LogLevel.Warn:
                    LogWarning(message);
                    break;
                case LogLevel.Error:
                    LogError(message);
                    break;
                case LogLevel.Exception:
                    Log(new Exception(message));
;                    break;
            }
        }

        public void LogDebug(string message) { if (LogLevel > LogLevel.Info) return; Log("Debug", message); }
        public void LogDebug(IEnumerable<string> messages) { if (LogLevel > LogLevel.Info) return; Log("Debug", messages); }
        public void LogInformation(string message) { if (LogLevel > LogLevel.Info) return; Log("Info", message); }
        public void LogInformation(IEnumerable<string> messages) { if (LogLevel > LogLevel.Info) return; Log("Info", messages); }
        public void LogWarning(string message) { if (LogLevel > LogLevel.Warn) return; Log("Warn", message); }
        public void LogWarning(IEnumerable<string> messages) { if (LogLevel > LogLevel.Warn) return; Log("Warn", messages); }
        public void LogError(string message) { if (LogLevel > LogLevel.Error) return; Log("Error", message); }
        public void LogError(IEnumerable<string> messages) { if (LogLevel > LogLevel.Info) return; Log("Error", messages); }

        internal abstract void Log(string message);

        protected abstract void Log(string logtype, IEnumerable<string> messages);
        protected abstract void Log(string logtype, string message);
        public abstract void Log(Exception ex);
        public abstract string FileExtension();

    }
}
