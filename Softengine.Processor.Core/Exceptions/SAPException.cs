﻿using System;

namespace Softengine.Processor.Core.Exceptions
{
    public class SAPException : Exception
    {
        private readonly SAPbobsCOM.Company _company;

        public SAPException(SAPbobsCOM.Company company)
        {
            _company = company;
            ErrorMessage = _company.GetLastErrorDescription();
            Code = _company.GetLastErrorCode();
        }
        public override string Message
        {
            get { return ToString(); }
        }

        public override string ToString()
        {
            return $"Error {Code} - {ErrorMessage}";
        }

        public int Code { get; set; }
        public string ErrorMessage { get; set; }
    }
}
