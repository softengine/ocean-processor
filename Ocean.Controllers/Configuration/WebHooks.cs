﻿using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace Ocean.Controllers.Configuration
{
    [XmlRoot(ElementName = "WebHooks")]
    public class WebHooks
    {
        [XmlAttribute(AttributeName = "url")]
        public string Url { get; set; }
        [XmlAttribute(AttributeName = "body")]
        public string Body { get; set; }

        public static WebHooks Instance()
        {
            try
            {
                var path = Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().ManifestModule.Name, "");
                path = Path.Combine(path, "Configuration.xml");

                var serializer = new XmlSerializer(typeof(WebHooks));
                using (var fileStream = new FileStream(path, FileMode.Open))
                {
                    return (WebHooks)serializer.Deserialize(fileStream);
                }

            }
            catch 
            {
                return new WebHooks();
            }
        }
    }
}
