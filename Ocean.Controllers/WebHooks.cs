﻿using System;
using Softengine.Processor.Core.Queue;
using System.Linq;
using System.Net;
using System.IO;

namespace Ocean.Controllers
{
    public class WebHooks
    {
        public static void Call(Message message)
        {
            try
            {
                SubmitRequest(message.Action, message.Key);
                message.Success();
            }
            catch (Exception ex)
            {
                message.Logger.Log(ex);
                message.Failed(-1, $"Error with Purchase Order entry {message.Key}: {ex.Message}");
            }
        }

        private static void SubmitRequest(string type, string key)
        {
            var config = Configuration.WebHooks.Instance();
            //Configuration.Mapping mapping = config.Mappings.List.Where(x => x.Type == type).FirstOrDefault();
            //if (mapping == null) throw new Exception($"Unabled to find Webhook mapping for type {type}");


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(config.Url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var json = config.Body.Replace("[0]", type).Replace("[1]", key);
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
        }
    }
}
